import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:unit_6_module_3/pages/home.dart';

import 'apis/apis.dart';

void main() => runApp(
      Provider<Apis>(
        builder: (_) => Apis(),
        child: MaterialApp(
          theme: ThemeData(primaryColor: Colors.blue),
          home: Home(),
        ),
      ),
    );
