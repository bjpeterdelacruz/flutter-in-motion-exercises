import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../apis/apis.dart';
import '../apis/contact.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var apis = Provider.of<Apis>(context);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Flutter in Motion'),
      ),
      body: FutureBuilder<List<Contact>>(
        future: apis.retrieve(),
        builder: (_, AsyncSnapshot<List<Contact>> snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          if (snapshot.hasError) {
            return Center(
              child: Text(snapshot.error.toString()),
            );
          }
          var contacts = snapshot.data;
          if (contacts.length == 0) {
            return Center(
              child: const Text("No Records"),
            );
          }
          return ListView.builder(
            itemCount: contacts.length,
            itemBuilder: (_, int index) {
              var contact = contacts[index];
              return ListTile(
                leading: Image.network(contact.url),
                title: Text(contact.fullName),
                contentPadding: EdgeInsets.all(5.0),
              );
            }
          );
        }
      ),
    );
  }
}
